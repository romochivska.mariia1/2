﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ШП_ЛР1
{
    public class Hotel
    {
        public int RoomNumber { get; set; }
        public int Floor { get; set; }
        public int Capacity { get; set; }
        public double DailyRate { get; set; }
    }

    public class Tenant
    {
        public string LastName { get; set; }
        public int RoomNumber { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
    }

    public class Program
    {
        public static void Main()
        {
            List<Hotel> hotels = new List<Hotel>
        {
            new Hotel { RoomNumber = 101, Floor = 1, Capacity = 2, DailyRate = 100.0 },
            new Hotel { RoomNumber = 102, Floor = 1, Capacity = 3, DailyRate = 120.0 },
            new Hotel { RoomNumber = 103, Floor = 1, Capacity = 2, DailyRate = 110.0 },
            new Hotel { RoomNumber = 104, Floor = 1, Capacity = 3, DailyRate = 130.0 },
            new Hotel { RoomNumber = 105, Floor = 2, Capacity = 2, DailyRate = 105.0 },
            new Hotel { RoomNumber = 106, Floor = 2, Capacity = 3, DailyRate = 125.0 },
            new Hotel { RoomNumber = 107, Floor = 2, Capacity = 2, DailyRate = 95.0 },
            new Hotel { RoomNumber = 108, Floor = 2, Capacity = 3, DailyRate = 115.0 },
            new Hotel { RoomNumber = 109, Floor = 3, Capacity = 2, DailyRate = 99.0 },
            new Hotel { RoomNumber = 110, Floor = 3, Capacity = 3, DailyRate = 119.0 },
            new Hotel { RoomNumber = 111, Floor = 3, Capacity = 2, DailyRate = 98.0 },
            new Hotel { RoomNumber = 112, Floor = 3, Capacity = 3, DailyRate = 118.0 },
            new Hotel { RoomNumber = 113, Floor = 4, Capacity = 2, DailyRate = 96.0 },
            new Hotel { RoomNumber = 114, Floor = 4, Capacity = 3, DailyRate = 116.0 },
            new Hotel { RoomNumber = 115, Floor = 4, Capacity = 2, DailyRate = 97.0 },
            new Hotel { RoomNumber = 116, Floor = 4, Capacity = 3, DailyRate = 117.0 },
            new Hotel { RoomNumber = 117, Floor = 5, Capacity = 2, DailyRate = 94.0 },
            new Hotel { RoomNumber = 118, Floor = 5, Capacity = 3, DailyRate = 114.0 },
            new Hotel { RoomNumber = 119, Floor = 5, Capacity = 2, DailyRate = 93.0 },
            new Hotel { RoomNumber = 120, Floor = 5, Capacity = 3, DailyRate = 100.0 },
        };

            List<Tenant> tenants = new List<Tenant>
        {
            new Tenant { LastName = "Кравець", RoomNumber = 101, CheckInDate = DateTime.Parse("2023-10-01"), CheckOutDate = DateTime.Parse("2023-10-05") },
            new Tenant { LastName = "Яновська", RoomNumber = 102, CheckInDate = DateTime.Parse("2023-09-15"), CheckOutDate = DateTime.Parse("2023-09-20") },
            new Tenant { LastName = "Петренко", RoomNumber = 103, CheckInDate = DateTime.Parse("2023-08-20"), CheckOutDate = DateTime.Parse("2023-08-25") },
            new Tenant { LastName = "Коваль", RoomNumber = 101, CheckInDate = DateTime.Parse("2023-07-10"), CheckOutDate = DateTime.Parse("2023-07-15") },
            new Tenant { LastName = "iванов", RoomNumber = 101, CheckInDate = DateTime.Parse("2023-06-01"), CheckOutDate = DateTime.Parse("2023-06-05") },
            new Tenant { LastName = "Захаренко", RoomNumber = 106, CheckInDate = DateTime.Parse("2023-05-15"), CheckOutDate = DateTime.Parse("2023-05-20") },
            new Tenant { LastName = "Озерська", RoomNumber = 107, CheckInDate = DateTime.Parse("2023-04-20"), CheckOutDate = DateTime.Parse("2023-04-25") },
            new Tenant { LastName = "Мельник", RoomNumber = 108, CheckInDate = DateTime.Parse("2023-03-10"), CheckOutDate = DateTime.Parse("2023-03-15") },
            new Tenant { LastName = "Сiдоренко", RoomNumber = 109, CheckInDate = DateTime.Parse("2023-02-05"), CheckOutDate = DateTime.Parse("2023-02-10") },
            new Tenant { LastName = "Гриценко", RoomNumber = 110, CheckInDate = DateTime.Parse("2023-01-15"), CheckOutDate = DateTime.Parse("2023-01-20") },
            new Tenant { LastName = "Козлов", RoomNumber = 111, CheckInDate = DateTime.Parse("2022-12-20"), CheckOutDate = DateTime.Parse("2022-12-25") },
            new Tenant { LastName = "Павленко", RoomNumber = 112, CheckInDate = DateTime.Parse("2022-11-10"), CheckOutDate = DateTime.Parse("2022-11-15") },
            new Tenant { LastName = "Литвиненко", RoomNumber = 113, CheckInDate = DateTime.Parse("2022-10-05"), CheckOutDate = DateTime.Parse("2022-10-10") },
            new Tenant { LastName = "Бойко", RoomNumber = 117, CheckInDate = DateTime.Parse("2022-09-15"), CheckOutDate = DateTime.Parse("2022-09-20") },
            new Tenant { LastName = "Кравченко", RoomNumber = 116, CheckInDate = DateTime.Parse("2022-08-20"), CheckOutDate = DateTime.Parse("2022-08-25") },
            new Tenant { LastName = "Семененко", RoomNumber = 116, CheckInDate = DateTime.Parse("2022-07-10"), CheckOutDate = DateTime.Parse("2022-07-15") },
            new Tenant { LastName = "Поляков", RoomNumber = 117, CheckInDate = DateTime.Parse("2022-06-01"), CheckOutDate = DateTime.Parse("2022-06-05") },
            new Tenant { LastName = "Коваленко", RoomNumber = 118, CheckInDate = DateTime.Parse("2022-05-15"), CheckOutDate = DateTime.Parse("2022-05-20") },
            new Tenant { LastName = "Дорош", RoomNumber = 119, CheckInDate = DateTime.Parse("2022-04-20"), CheckOutDate = DateTime.Parse("2022-04-25") },
            new Tenant { LastName = "Гаврилюк", RoomNumber = 120, CheckInDate = DateTime.Parse("2022-03-10"), CheckOutDate = DateTime.Parse("2022-03-15") }
        };
            DateTime targetDate = DateTime.Parse("2023-10-01");

            TaskA(tenants, targetDate);
            TaskB(hotels, tenants);
            TaskC(hotels,tenants);
        }
       
        public static void TaskA(List<Tenant> tenants, DateTime targetDate)
        {
            var selectedTenants = tenants.Where(tenant =>
                tenant.CheckInDate <= targetDate && tenant.CheckOutDate >= targetDate).ToList();

            int countOfTenants = selectedTenants.Count;
            Console.WriteLine($"Кiлькiсть жильцiв, якi перебувають в готелi протягом заданого дня: {countOfTenants}");
        }
       
        

        public static void TaskB(List<Hotel> hotels, List<Tenant> tenants)
        {
            var floors = hotels.Select(hotel => hotel.Floor).Distinct();
            foreach (var floor in floors)
            {
                var totalAmount = tenants
                    .Where(tenant => hotels.Any(hotel => hotel.Floor == floor && hotel.RoomNumber == tenant.RoomNumber))
                    .Sum(tenant => (tenant.CheckOutDate - tenant.CheckInDate).Days * hotels.First(hotel => hotel.RoomNumber == tenant.RoomNumber).DailyRate);

                Console.WriteLine($"Сумарну грошова сума, яку мають сплатити за добу вже поселенi на поверсi {floor}: ${totalAmount}");
            }
        }


        public static void TaskC(List<Hotel> hotels, List<Tenant> tenants)
        {
            var uniqueFloors = hotels.Select(hotel => hotel.Floor).Distinct();

            Console.WriteLine("Поверхи, якi заповненi на половину або бiльше:");

            foreach (var floor in uniqueFloors)
            {
                var floorCapacity = hotels
                    .Where(hotel => hotel.Floor == floor)
                    .Select(hotel => hotel.Capacity)
                    .Sum();

                var occupiedRooms = hotels
                    .Where(hotel => hotel.Floor == floor)
                    .Join(tenants, hotel => hotel.RoomNumber, tenant => tenant.RoomNumber, (hotel, tenant) => tenant)
                    .Count();

                if (occupiedRooms >= floorCapacity / 2)
                {
                    Console.WriteLine($"Поверх {floor}");
                }
            }
        }



    }
}
